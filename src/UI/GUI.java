package UI;

import Domain.FakeClock;
import Domain.Simulation;

import java.awt.Color;
import java.awt.Font;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import java.awt.event.*;

public class GUI extends JPanel implements ActionListener, ChangeListener
{
    static final long serialVersionUID = -1255148347247167282L;
	private JTextField minArivalTime;
	private JTextField maxArivalTime;
	private JTextField minServingTime;
	private JTextField maxServingTime;
	private JTextField openingTime;
	private JTextField closingTime;
	private JTextField queuesNumber;
	private JTextArea  logger;
	private JButton startButton;
	private GraphicRepresentation drawPanel;
	private Simulation sim = null;
	private JFrame wrapper;
	private JLabel labelQueuesNumber = new JLabel ("Numar cozi:");
	private JLabel clock = new JLabel("Clock: N/A");
	private JLabel labelArivalTime= new JLabel ("Interval sosire:");
	private JLabel labelServingTime = new JLabel ("Interval servire :");
	private JLabel labelWorkingHours = new JLabel ("Orar :");
	private JSlider refreshRate = null;
	
	public GUI() 
	{
		wrapper = new JFrame ("Simulation ");
		
		

		wrapper.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);	
		wrapper.setSize(1280, 720);
		
		labelArivalTime.setBounds(20,10,100,25);
		labelServingTime.setBounds(130,10,100,25);
		labelWorkingHours.setBounds(240,10,100,25);
		labelQueuesNumber.setBounds(350,10,100,25);
		
		minArivalTime = new JTextField();	
		minArivalTime.setBounds(10,35,45,25);
		
		maxArivalTime = new JTextField();
		maxArivalTime.setBounds(65,35,45,25);
		
		minServingTime = new JTextField();
		minServingTime.setBounds(130,35,45,25);
		
		maxServingTime = new JTextField();
		maxServingTime.setBounds(185,35,45,25);
		
		openingTime = new JTextField();
		openingTime.setBounds(240,35,45,25);
		
		closingTime = new JTextField();
		closingTime.setBounds(295,35,45,25);
		
		queuesNumber = new JTextField();
		queuesNumber.setBounds(350,35,100,25);
		
		logger = new JTextArea();
		JScrollPane loggerScroller = new JScrollPane(logger);
		loggerScroller.setBounds(10,65,400,600);
		drawPanel = new GraphicRepresentation();
		
		startButton = new JButton("Simulate");
		startButton.setBounds(790,10,100,50);
		startButton.setBackground(Color.GREEN);
		startButton.addActionListener(this);
		
		drawPanel = new GraphicRepresentation();
		drawPanel.setBackground(Color.WHITE);
		
		clock.setBounds(1000,10,200,50);
		clock.setFont(new Font(clock.getFont().getFontName(),Font.PLAIN,15));
		
		JScrollPane drawScroller = new JScrollPane(drawPanel);
		drawScroller.setBounds(420,65,840,600);
		
		refreshRate = new JSlider(JSlider.HORIZONTAL,
                0, 200, 100);
		refreshRate.addChangeListener(this);
		
		refreshRate.setSnapToTicks(true);
		refreshRate.setBounds(470,10,300,50);
		refreshRate.setMajorTickSpacing(50);
		refreshRate.setMinorTickSpacing(10);
		refreshRate.setPaintTicks(true);
		refreshRate.setPaintLabels(true);
		
		
		wrapper.add(labelQueuesNumber);
		wrapper.add(labelArivalTime);
		wrapper.add(labelServingTime);
		wrapper.add(labelWorkingHours);
		wrapper.add(minArivalTime);
		wrapper.add(maxArivalTime);
		wrapper.add(minServingTime);
		wrapper.add(maxServingTime);
		wrapper.add(openingTime);
		wrapper.add(closingTime);
		wrapper.add(queuesNumber);
		wrapper.add(startButton);
		wrapper.add(loggerScroller);
		wrapper.add(drawScroller);
		wrapper.add(refreshRate);
		wrapper.add(clock);
		
		wrapper.setResizable(true);
		wrapper.getContentPane().setLayout(null);
		wrapper.setLocationRelativeTo(null);
		wrapper.setVisible(true);
	}
	
	public void println(String str) 
	{
		logger.append(str + "\n");
		logger.setCaretPosition( logger.getText().length() );
	}
   
    public void incrementClients(int queue)
    {
    	drawPanel.incrementClientCount(queue);
    }
    
    public void decrementClients(int queue)
    {
    	drawPanel.decrementClientCount(queue);    	
    }
    
    public void updateClock(FakeClock c)
    {
    	clock.setText("Clock: " + c.toString()); 	
    }
    
	@Override
	// button listener
	public void actionPerformed(ActionEvent arg0)
	{
		int minSerTime = Integer.parseInt(this.minServingTime.getText());
		int maxSerTime = Integer.parseInt(this.maxServingTime.getText());
		int minAriTime = Integer.parseInt(this.minArivalTime.getText());
		int maxAriTime = Integer.parseInt(this.maxArivalTime.getText());
		int openingTime = Integer.parseInt(this.openingTime.getText());
		int closingTime = Integer.parseInt(this.closingTime.getText());
		int nrQueues = Integer.parseInt(this.queuesNumber.getText());
		// sleep time 
		int refreshRate = 1000 / (this.refreshRate.getValue() + 1);
		drawPanel.setNrQueues(nrQueues);
		if(sim != null) 
		{
			try 
			{
				sim.stopQueues();
				sim.join();
			} catch (InterruptedException e) 
			{
				e.printStackTrace();
			}
		}
		
		this.logger.setText("");
		sim = new Simulation(minSerTime,maxSerTime,openingTime,closingTime,nrQueues,minAriTime,maxAriTime,this,refreshRate);
		sim.start();
	}
	
	// for slider
	@Override
	public void stateChanged(ChangeEvent e) 
	{
		JSlider source = (JSlider)e.getSource();
	    int refreshRate = (int)source.getValue();
	    if(sim != null)
	     	sim.setRefreshRate(1000 / (refreshRate + 1));
		
	} 
	
	public static void main(String args[]) 
    {
        new GUI ();
    }
}
