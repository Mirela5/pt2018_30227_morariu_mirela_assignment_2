package UI;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

// Cu rolul in definirea formelor pentru reprezentarea grafica a caselor si a clientilor
public class GraphicRepresentation extends JPanel
{

	private static final long serialVersionUID = 2788799729622356554L;
	private int[] queues;
	private int queuesNr;
    
	// construirea formelor
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		g.setColor(Color.RED);
		for(int j = 0; j < queuesNr; j++) 
		{
			g.fillRect(35 + j * 100,25,70,30);
			for(int i = 1; i <= queues[j] ; i++) 
			{
				g.fillOval(55 + j * 100 , i * 35 + 50 , 20, 20);
			}
		}	
	}
	
	// incrementarea , decrementarea numarului de clienti dintr-o coada
    public void incrementClientCount(int queue)
    {
    	this.queues[queue]++;
    	repaint();
    }
    
    public void decrementClientCount(int queue)
    {
    	this.queues[queue]--;
    	repaint();
    }
    
    // numarul de case deschise
    public void setNrQueues(int nrQueues) 
    {
    	this.queuesNr = nrQueues;
    	queues = new int[nrQueues];
    	for(int i = 0; i < nrQueues; i++)
    	{
    		queues[i] = 0;
    	}
    	repaint();
    }
    
    
}