package Domain;

public class FakeClock extends Thread implements Runnable 
{
	int seconds;
	int refreshRate;
	private boolean run_flag;
	
	public FakeClock(int seconds, int refreshRate) 
	{
		this.refreshRate = refreshRate;
		this.seconds = seconds;
		this.run_flag = true;
	}
	
	public int getSeconds() 
	{
		return seconds;
	}
	
	public void increment() 
	{
		this.seconds += 1;
	}
	
	public int getRefreshRate() 
	{
		return this.refreshRate;
	}
	
	public void setRefreshRate(int refreshRate) 
	{
		this.refreshRate = refreshRate;
	}
	
	public String toString() 
	{
		int time = this.getSeconds();
		// hh:mm:ss
		String toReturn = "";
		if(time / 3600 < 10) {
			toReturn += "0";
		}
		toReturn += (time / 3600) + ":";
		time %= 3600;
		if(time / 60 < 10) {
			toReturn += "0";
		}
		toReturn += (time / 60 ) + ":";
		time %= 60;
		if(time < 10) {
			toReturn += "0";
		}
		toReturn += time  + " ";
		return toReturn;
	}
	
	public void run() 
	{
		while(run_flag) 
		{
			this.increment();
			try
			{
				sleep(refreshRate);
			} 
			catch (InterruptedException e)
			{
				this.run_flag = false;
				System.out.println("Execution stopped because of a clock error");
			}
		}
	}
}
