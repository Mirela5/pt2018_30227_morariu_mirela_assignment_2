package Domain;

import java.util.ArrayList;
import java.util.Random;

import UI.GUI;;

public class Queue extends Thread implements Runnable 
{
	private FakeClock clock;
	
	private int lastServedTime = 0;
	private int minServingTime = 0;
	private int maxServingTime = 0;
	private int currentServingTime = 0;
	private int closingTime;
	private int id;
	
	private ArrayList<Client> queue;
	// indexul clientului actual care asteapta sa fie servit
	private int clientBeingServed;
	private Random nrGenerator;
	GUI gui;
	
	public Queue(int id, FakeClock c, int minServingTime, int maxServingTime, int closingTime, GUI g) 
	{
		this.id = id;
		this.clock = c;
		this.lastServedTime = c.getSeconds();
		this.nrGenerator = new Random();
		this.minServingTime = minServingTime;
		this.maxServingTime = maxServingTime;
		this.closingTime = closingTime * 3600 ; 
		this.currentServingTime = nrGenerator.nextInt((maxServingTime - minServingTime ) + 1) + minServingTime;
		this.queue = new ArrayList<>();
		this.clientBeingServed = 0;
		this.gui = g;
	}
	
	public void run()
	{
		gui.println("[QUEUE" + id + "] Started at " + clock.toString());
		boolean runningFlag = true;
		
		while(runningFlag) 
		{
			// daca timpul curent e mai mare decat timpul la care ar fi trebuit sa se serveasca
			if(this.clock.getSeconds() >= this.lastServedTime + this.currentServingTime 
					&& clientBeingServed < queue.size()  ) 
			{
				gui.println("[QUEUE" + id + "] Client Served at " + clock.toString());
				gui.decrementClients(id);
				
				this.currentServingTime = nrGenerator.nextInt((maxServingTime - minServingTime ) + 1) + minServingTime;
				this.lastServedTime = clock.getSeconds();	
				this.queue.get(clientBeingServed).finishTime = clock.getSeconds();
				
				// urmatorul client
				this.clientBeingServed++;
			}
			try 
			{
				sleep(clock.getRefreshRate());
			} 
			catch (InterruptedException e) 
			{
				gui.println("Execution stopped because of a clock error");
				break;
			}
			runningFlag = false;
			
			if(this.getClientCount() != 0 || this.clock.getSeconds() < closingTime) 
			{
					runningFlag = true;
			}
		}		
	}
	
	public void addClient(Client c) 
	{
		this.queue.add(c);
	}
	
	public int getClientCount() 
	{
		return this.queue.size() - clientBeingServed ;
	}
	

}
