package Domain;

import java.util.ArrayList;
import java.util.Random;

import UI.GUI;

public class Simulation extends Thread implements Runnable{
	private ArrayList<Queue> queues;
	private Random nrGenerator;
	private int closingTime;
	@SuppressWarnings("unused")
	private int minServingTime;
	@SuppressWarnings("unused")
	private int maxServingTime;
	private int minArivalTime;
	private int maxArivalTime;
	private int lastArival;
	private int currentArivalTime;
	private int nextClientId;
	private FakeClock clock;
	private GUI gui;
	
	public Simulation(int minServingTime, int maxServingTime, int openingTime , int  closingTime, int nrQueues,
		int minArivalTime, int maxArivalTime, GUI gui,int refreshRate) {
		
		// set defaults
		this.lastArival = 0;
		this.nrGenerator = new Random();
		this.currentArivalTime = nrGenerator.nextInt(maxArivalTime - minArivalTime + 1) + minArivalTime;  
		this.nextClientId = 0;
		this.queues = new ArrayList<>();
		this.maxArivalTime = maxArivalTime;
		this.minArivalTime = minArivalTime;
		this.minServingTime = minServingTime;
		this.maxServingTime = maxServingTime;
		this.clock = new FakeClock(openingTime * 3600 ,refreshRate);// transform from hours to seconds
		this.closingTime = closingTime * 3600 ; // transform from hours to seconds
		for(int i = 0 ; i < nrQueues ; i++) {
			this.queues.add(new Queue(i,clock, minServingTime, maxServingTime, closingTime,gui));
			this.queues.get(i).start();
		}
		this.gui = gui;
		
		
	}
	public void setRefreshRate(int refreshRate) {
		clock.setRefreshRate(refreshRate);
	}
	public void run() {
		int queueToAddClientTo;
		this.clock.start();
		boolean runningFlag = true;
		while(runningFlag) {
				
			if(this.clock.getSeconds() < closingTime && this.clock.getSeconds() > lastArival + currentArivalTime) {
				// generate random number 
				queueToAddClientTo = nrGenerator.nextInt(queues.size() );
				// add client to that random queue
				queues.get(queueToAddClientTo).addClient(new Client(nextClientId,clock.getSeconds()));
				nextClientId++;
				this.currentArivalTime = nrGenerator.nextInt(maxArivalTime - minArivalTime + 1) + minArivalTime;  
				this.lastArival = clock.getSeconds(); 
				gui.println("[MAIN] Client arrived at " + clock.toString() 
				+ " and joined QUEUE " + queueToAddClientTo );
				gui.incrementClients(queueToAddClientTo);
			}
			
			
			runningFlag = false;
			for(Queue i: this.queues) {
				if(i.getClientCount() != 0 || this.clock.getSeconds() < closingTime) {
					runningFlag = true;
				}
			
			}
			
			try {
				sleep(clock.getRefreshRate());
			} catch (InterruptedException e) {
				gui.println("Could not wait");
				e.printStackTrace(); 
			}
			gui.updateClock(clock);
			
		}
		
		gui.println("[MAIN] Shop Closed");
		this.stopQueues();

	}
	public void stopQueues() {
		try {
			clock.join();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for(Queue i : this.queues)
			try {
				i.join();
				
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
